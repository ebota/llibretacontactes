<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Test DB</title>
</head>

<h1>Connectant</h1>
<?php

$servername = "localhost";
$username = "admin_a1";
$password = "aaaaaa";
$dbname = "admin_projecte";

// Create connection
$conn = mysqli_connect($servername, $username, $password, $dbname);
// Check connection
if (!$conn) {
    die("Connection failed: " . mysqli_connect_error());
}
echo "<p>Ok</p>";

?>
<h1>Dades i mes</h1>

<?php
$sql = "SELECT id, nom, telefon FROM test";
$result = mysqli_query($conn, $sql);

#Per mostrar els errors del select, si n'hi hagués
if (mysqli_error($conn)){
	echo "<p>Error: ".mysqli_error($conn).".</p>";
}

if (mysqli_num_rows($result) > 0) {
    // output data of each row
    while($row = mysqli_fetch_assoc($result)) {
        echo "id: " . $row["id"]. " - Name: " . $row["nom"]. " - Telef: " . $row["telefon"]. "<br>";
    }
} else {
    echo "0 results";
}

mysqli_close($conn);
?>

<h1>-eof-</h1>
</html>